# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

# +
import numpy as np
import os
import sys; sys.path.insert(1, './classes/')
import matplotlib.pyplot as plt; plt.style.use('ggplot')
import time

from elastostatic import *
from tools import *

tol = 1e-10
components = ['V', 'N', 'E']
nature = ['Re', 'Im']
volumetric_output = False

# +
a = elastostatic_solver(meshfile = "../meshes/elastostatic/mesh.h5")
a.config.run.tensor_order = 4
a.construct_mesh(with_surface_topo = False, 
                 with_moho_topo = False, 
                 oneD_model="prem_iso_one_crust",
                 nex = 30, 
                 buffer = 0, 
                 local_refinement_level=2, 
                 global_refinement_level=2)

sph_crd = get_sph_coord(a.mesh)
a.prepare_mesh_fields(real_or_imag = 're')

def dist(x):
    #return np.abs(1221501. - np.linalg.norm(x, axis=-1))
    return np.abs(3480000. - np.linalg.norm(x, axis=-1))
a.mesh.find_side_sets_generic("r0", dist, tolerance=10.)

a.write_mesh(path = a.meshfile)
# -

indices = [str(ind) for ind in regular_grid[:,2].astype(int)]
side_points = get_gps_stations(indeces = indices, latitudes = regular_grid[:,0], longitudes = regular_grid[:,1],)

receivers = {f'{station.name}':{'SidePoint': station} for station in side_points}

# +
with open("../stations/cn_OceanOnly_GOT410c-M2_cm_convgf_GOT410c_global_PREM.txt") as file:
    data = np.array([line.split() for line in file.readlines()])
header = data[0]
regular_grid = data[1:].astype(float)

indices     = [str(ind) for ind in regular_grid[:,0].astype(int)]
east_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,3], regular_grid[:,4])])/1000.
north_ri    = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,5], regular_grid[:,6])])/1000.
vert_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,7], regular_grid[:,8])])/1000.
side_points = get_gps_stations(indeces = indices, latitudes = regular_grid[:,1], longitudes = regular_grid[:,2],)
# -

receivers = {f'{station.name}':{'SidePoint': station, 
                                'lat': lat, 'lon': lon,   
                                'E-Re-LD': e_re, 'E-Im-LD': e_im, 
                                'N-Re-LD': n_re, 'N-Im-LD': n_im, 
                                'V-Re-LD': v_re, 'V-Im-LD': v_im,
                                'E-Ab-LD': e_ab, 'E-Ph-LD': e_ph,
                                'N-Ab-LD': n_ab, 'N-Ph-LD': n_ph,
                                'V-Ab-LD': v_ab, 'V-Ph-LD': v_ph} 
             for station,
                 lat, lon,
                 e_re, e_im, 
                 n_re, n_im, 
                 v_re, v_im,
                 e_ab, e_ph,
                 n_ab, n_ph,
                 v_ab, v_ph
             in 
             zip(side_points,
                 regular_grid[:,1], regular_grid[:,2],
                 east_ri [:,0], east_ri [:,1], 
                 north_ri[:,0], north_ri[:,1], 
                 vert_ri [:,0], vert_ri [:,1],
                 regular_grid[:,3], regular_grid[:,4],
                 regular_grid[:,5], regular_grid[:,6],
                 regular_grid[:,7], regular_grid[:,8])
            }

# **Running real part simulation**

# +
a.prepare_mesh_fields(real_or_imag = 're')

a.config.elastos_run.relative_tolerance = 1e-10
a.config.run.ranks                      = 120
a.config.run.wall_time_in_seconds       = 3600
a.config.elastos_run.max_iterations     = 6000
a.config.elastos_run.point_solution     = a.config.elastos_run.solution_folder + "receivers_re.h5"
a.config.elastos_run.volume_solution    = a.config.elastos_run.solution_folder +  "solution_re.h5"
a.config.run.site_name                  = 'daint'
sim_re                                  = a.run_simulation(stations = side_points)

a.mesh.attach_field('NEUMANN_re', a.mesh.elemental_fields['NEUMANN'])
# -

with open('../solutions/elastostatic/stdout') as file:
    data      = np.array(file.readlines())
    begin     = np.where(data == '----- BEGIN ITERATION LOG -----\n')[0][0] + 5
    end       = np.where(data == '----- END ITERATION LOG -----\n')[0][0]
    data      = data[begin:end]
    rel_error = np.array([line.split() for line in data], dtype=np.float64)
plt.figure(figsize=(20, 4))
plt.semilogy(rel_error[:,0], rel_error[:,2])
plt.xlabel('Iteration')
plt.ylabel('Rel.error^2')
print('Minimal rel.error^2:', np.min(rel_error))
plt.savefig('convergence.png')

# **Reading the point solution and add to the mesh**

filepath = "../solutions/elastostatic/receivers_re.h5"
indices_op, results = read_receiver_results(filepath)
for i, ind in enumerate(indices_op):
    for j, comp in enumerate(components):
        receivers[ind.decode()][f'{comp}-Re-SLV'] = results[i, j]

# **Reading the solution, rotate it and add to the mesh**

if volumetric_output:
    a.config.elastos_run.volume_solution = a.config.elastos_run.solution_folder +  "solution_re.h5"
    displacement_re = read_volumetric_results(a.config.elastos_run.volume_solution)

    shape = displacement_re.shape
    displ_temp = displacement_re.reshape((shape[0]*shape[1], 3))
    lat_flat   = sph_crd[:,:,0] .reshape((shape[0]*shape[1]))
    lon_flat   = sph_crd[:,:,1] .reshape((shape[0]*shape[1]))
    displacement_re_rot = np.array([get_rotation_matrix(lat, lon).dot(vec) for lat,lon,vec in zip(lat_flat, lon_flat, displ_temp)]).reshape(shape)

    for i, comp in enumerate(components):
        a.mesh.attach_field(f'displacement_{comp}_re', displacement_re_rot[:,:,i] )

# **Launching imaginary part simulation**

# %pwd

a.prepare_mesh_fields(real_or_imag = "im")
a.config.elastos_run.point_solution  = a.config.elastos_run.solution_folder + "receivers_im.h5"
a.config.elastos_run.volume_solution = a.config.elastos_run.solution_folder +  "solution_im.h5"
a.write_mesh(path = a.meshfile)
sim_im = a.run_simulation(stations = side_points)

with open('../solutions/elastostatic/stdout') as file:
    data      = np.array(file.readlines())
    begin     = np.where(data == '----- BEGIN ITERATION LOG -----\n')[0][0] + 5
    end       = np.where(data == '----- END ITERATION LOG -----\n')[0][0]
    data      = data[begin:end]
    rel_error = np.array([line.split() for line in data], dtype=np.float64)
plt.figure(figsize=(20, 4))
plt.semilogy(rel_error[:,0], rel_error[:,2])
plt.xlabel('Iteration')
plt.ylabel('Rel.error^2')
print('Minimal rel.error^2:', np.min(rel_error))
plt.savefig('convergence.png')

# **Reading the solution, rotate it and add to the mesh**

if volumetric_output:
    a.config.elastos_run.volume_solution = a.config.elastos_run.solution_folder +  "solution_im.h5"
    displacement_im = read_volumetric_results(a.config.elastos_run.volume_solution)

    shape = displacement_im.shape
    displ_temp = displacement_im.reshape((shape[0]*shape[1], 3))
    lat_flat   = sph_crd[:,:,0] .reshape((shape[0]*shape[1]))
    lon_flat   = sph_crd[:,:,1] .reshape((shape[0]*shape[1]))
    displacement_im_rot = np.array([get_rotation_matrix(lat, lon).dot(vec) for lat,lon,vec in zip(lat_flat, lon_flat, displ_temp)]).reshape(shape)
    for i, comp in enumerate(components):
        a.mesh.attach_field(f'displacement_{comp}_im', displacement_im_rot[:,:,i] )

# **Forming complex displacement array**

if volumetric_output:
    displacement_cpx = np.zeros_like(displacement_re_rot,dtype=np.complex64)
    displacement_cpx.real = displacement_re_rot
    displacement_cpx.imag = displacement_im_rot
    for i, comp in enumerate(components):
        a.mesh.attach_field(f'disp_absolute_{comp}' , np.absolute(displacement_cpx[:,:,i]))
        a.mesh.attach_field(f'disp_phase_{comp}'   , np.angle(displacement_cpx[:,:,i]))
    a.mesh.write_h5('result.h5')

# **Reading the point solution, rotate it and add to the mesh**

for k, nat in enumerate(nature):
    filename = f"../solutions/elastostatic/receivers_{nat.lower()}.h5"
    indices_op, results = read_receiver_results(filename)
    for i, ind in enumerate(indices_op):
        for j, comp in enumerate(components):
            receivers[ind.decode()][f'{comp}-{nat}-SLV'] = results[i, j]

for i, ind in enumerate(indices_op):
    for j, comp in enumerate(components):
        for k, nat in enumerate(['Re', 'Im']): 
            receivers[ind.decode()][f'{comp}-{nat}-Rel.Misfit'] = (receivers[ind.decode()][f'{comp}-{nat}-SLV'] - receivers[ind.decode()][f'{comp}-{nat}-LD'])

receivers_backup = receivers.copy()



header

import pygmt

# +
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(nature):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-SLV']*1000. for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'Salvus_{nat}_{comp}')
plt.savefig('salvus.png')

# + tags=[]
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(nature):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-LD']*1000. for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'LoadDef_{nat}_{comp}')
plt.savefig('loaddef.png')

# +
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(nature):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-Rel.Misfit']*1000. for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'Misfit_{nat}_{comp}')
plt.savefig('misfit.png')
# -



for i, ind in enumerate(indices_op):
    for j, comp in enumerate(components):
        ab, ph = compl2polar(receivers[ind.decode()][f'{comp}-Re-SLV'], receivers[ind.decode()][f'{comp}-Im-SLV'])
        ab_ld, ph_ld = compl2polar(receivers[ind.decode()][f'{comp}-Re-LD'], receivers[ind.decode()][f'{comp}-Im-LD'])
        receivers[ind.decode()][f'{comp}-Ab-SLV'] = ab*1000.
        receivers[ind.decode()][f'{comp}-Ph-SLV'] = np.degrees(ph)+180
        receivers[ind.decode()][f'{comp}-Ph-LD-test'] = np.degrees(ph_ld)



# + tags=[]
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(['Ab', 'Ph']):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-SLV'] for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'Salvus_{nat}_{comp}')

# +
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(['Ab', 'Ph']):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-LD'] for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'Loaddef_{nat}_{comp}')

# +
import pyshtools as pysh

order = 40
shexpans_LD = np.zeros((2, order+1, order+1))
field_to_expand = [receivers[f'XX.{ind}.'][f'V-Re-SLV']*1000. for ind in indices]
shexpans[:,:,:,i], misfit = pysh.shtools.SHExpandLSQ(field_to_expand, lat, lon, lmax=order)
# -
import pygmt


fig = pygmt.Figure()
fig.basemap(region="d", projection="R12c", frame="afg")
fig.coast(region="d", shorelines=True)
fig.show()

fig = pygmt.Figure()
fig.basemap(region=region, projection="M15c", frame=True)
fig.coast(land="black", water="skyblue")
pygmt.makecpt(cmap="viridis", series=[data.depth_km.min(), data.depth_km.max()])
fig.plot(
    x=data.longitude,
    y=data.latitude,
    sizes=0.02 * 2 ** data.magnitude,
    color=data.depth_km,
    cmap=True,
    style="cc",
    pen="black",
)
fig.colorbar(frame='af+l"Depth (km)"')
fig.show()


