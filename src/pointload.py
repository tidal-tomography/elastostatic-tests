# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

from salvus.mesh import simple_mesh
import salvus.namespace as sn
from salvus.flow import api, simple_config
from mass.elemental_matrices import get_mass_matrix
import numpy as np
import matplotlib.pyplot as plt; plt.style.use('ggplot')

tol      = 10
dim      = 3
R_Earth  = 6371e3
meshfile = './meshes/mesh.h5'
solutionfolder = './solution/'
solutionfile = 'solution.h5'


# +
def find_boundary_centre(mesh, tolerance=200000.):
    def dist(x):
        return np.linalg.norm(x, axis=-1)
    mesh.find_side_sets_generic("r0", dist, tolerance=tolerance)
    print('r0 has been found') if "r0" in mesh.side_sets.keys() else print('Resolution is not good enough to find this surface. Increase the tolerance')

def gauss(lat, lon, sigma, lat_0 = np.radians(90)):
    return np.exp(-0.5 * ( ((lat - lat_0)/sigma)**2/sigma**2 + (lon/sigma)**2/sigma**2) ) 

def read_solution_results(path, field_name_prefix = "solution", ):
    upload   = sn.UnstructuredMesh.from_h5(path)
    solution = np.zeros((upload.nelem, upload.nodes_per_element, 3))
    for i, j in enumerate(['_X', '_Y', '_Z']):
        solution[:,:,i] = upload.element_nodal_fields[field_name_prefix + j]
    return solution

def get_sph_coord(mesh):
    coords     = mesh.points.copy()
    longitudes = np.arctan2(coords[:,1], coords[:,0])
    latitudes  = np.arctan2(coords[:,2], np.linalg.norm(coords[:,0:2], axis=-1))
    radii      = np.linalg.norm(coords, axis=-1)
    sphcrd     = np.zeros((mesh.nelem, mesh.nodes_per_element, dim))
    sphcrd[:,:,0] = latitudes [mesh.connectivity]
    sphcrd[:,:,1] = longitudes[mesh.connectivity]
    sphcrd[:,:,2] = radii     [mesh.connectivity]
    return sphcrd

def get_lat_lon_fields_Ox(mesh):
    coords     = mesh.points.copy()
    longitudes = np.arctan2(coords[:,1], (-1)*coords[:,2])
    latitudes  = np.arctan2(coords[:,0], np.linalg.norm(coords[:,1:3], axis=-1))
    return latitudes[mesh.connectivity], longitudes[mesh.connectivity]

def get_rotation_matrix(lat, lon):
    return np.array([[ np.cos(lat)*np.cos(lon), np.cos(lat)*np.sin(lon), np.sin(lat)],
                     [-np.sin(lat)*np.cos(lon),-np.sin(lat)*np.sin(lon), np.cos(lat)],
                     [-np.sin(lon)            ,             np.cos(lon),           0]])

import pyshtools as pysh


# -

theta,phi = 73, 23
M = get_rotation_matrix(theta, phi)
M.dot(M.T)

# +
ms                       = simple_mesh.TidalLoading()
ms.basic.nex             = 24
ms.basic.model           = 'prem_iso_one_crust'
ms.advanced.tensor_order = 4
mesh = ms.create_mesh(verbose=True)

meshdim  = (mesh.nelem, mesh.nodes_per_element)
mass_matrix = get_mass_matrix(mesh)
lat_ox, lon_ox      = get_lat_lon_fields_Ox(mesh)

sph_crd = get_sph_coord(mesh)

zeros = np.zeros(meshdim)

# +
#tol      = 1
#north_pole_node = \
#np.where(  (mesh.points[mesh.connectivity][:,:,0] < (offset + tol))
#         * (mesh.points[mesh.connectivity][:,:,1] < (offset + tol))
#         * (mesh.points[mesh.connectivity][:,:,2] > (R_Earth-tol)) )

#NEUMANN = zeros.copy()
#NEUMANN[north_pole_node] = 1.#/mass_matrix[north_pole_node]
#mesh.attach_field('NEUMANN', NEUMANN)

# +
#tol = 0.0001
#lon_src = np.radians(0)
#lat_src = np.radians(90)
#source_indx = \
#np.where((latitude  > (lat_src - tol))
#        *(latitude  < (lat_src + tol))
#        *(longitude > (lon_src - tol))
#        *(longitude < (lon_src + tol))
#        *(radii     > (R_Earth - tol)))
#source_indx

# +
#NEUMANN = zeros.copy()
#NEUMANN[source_indx] = 1.#/mass_matrix[north_pole_node]
#mesh.attach_field('NEUMANN', NEUMANN)
# -

# Gaussian distribution

NEUMANN = zeros.copy()
surface_nodes = sph_crd[:,:,2] > (R_Earth - tol)
surface_condition = NEUMANN[surface_nodes]
surface_lat = sph_crd[:,:,0][surface_nodes]
surface_lon = sph_crd[:,:,1][surface_nodes]

sigma = 0.01
surface_condition = (-1)* np.array([gauss(lat, lon, sigma) for lat, lon in zip(surface_lat, surface_lon)])
NEUMANN[surface_nodes] = surface_condition
mesh.attach_field('NEUMANN', NEUMANN)

mesh.attach_field('zeros', zeros)
mesh.elemental_fields['fluid'] = np.zeros(mesh.nelem)
find_boundary_centre(mesh, tolerance = 500e3)
mesh.write_h5(meshfile)

# +
assert ("r0" in mesh.side_sets.keys())
assert ("r1" in mesh.side_sets.keys())
sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
sim.domain.polynomial_order                      = mesh.shape_order

sim.physics.elastostatic_equation.gravity                  = "full"
sim.physics.elastostatic_equation.right_hand_side.filename = meshfile
sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

sim.physics.elastostatic_equation.solution.filename        = solutionfile
sim.physics.elastostatic_equation.solution.fields          = ["solution", "residuals", "right-hand-side"]

boundaries = simple_config.boundary.Neumann(
    side_sets=["r1"]
)
sim.add_boundary_conditions(boundaries)

boundaries = simple_config.boundary.HomogeneousDirichlet(
    side_sets=['r0']
)
sim.add_boundary_conditions(boundaries)

sim.solver.max_iterations     = 10000
sim.solver.absolute_tolerance = 0.0
sim.solver.relative_tolerance = 5e-4
sim.solver.preconditioner     = True

sim.validate()

sn.api.run(
    input_file    = sim,
    site_name     = 'local',
    output_folder = solutionfolder,
    overwrite     = True,
    ranks         = 10,
)
# -

path             = solutionfolder + solutionfile
displacement     = read_solution_results(path)
displacement_amp = np.linalg.norm(displacement, axis=-1)

# +
mesh.attach_field('lat',    np.degrees(sph_crd[:,:,0]))
mesh.attach_field('lon',    np.degrees(sph_crd[:,:,1]))
mesh.attach_field('lat_ox', np.degrees(lat_ox))
mesh.attach_field('lon_ox', np.degrees(lon_ox))

mesh.attach_field('displacement_X', displacement[:,:,0])
mesh.attach_field('displacement_Y', displacement[:,:,1])
mesh.attach_field('displacement_Z', displacement[:,:,2])
mesh.attach_field('displacement_amp', displacement_amp)
# -

shape = displacement.shape
displ_temp =   displacement.reshape((shape[0]*shape[1], 3))
lat_flat   = sph_crd[:,:,0].reshape((shape[0]*shape[1]))
lon_flat   = sph_crd[:,:,1].reshape((shape[0]*shape[1]))
displacement_rot = np.array([get_rotation_matrix(lat, lon).dot(vec) for lat,lon,vec in zip(lat_flat, lon_flat, displ_temp)]).reshape(shape)

mesh.attach_field('displacement_v', displacement_rot[:,:,0] )
mesh.attach_field('displacement_e', displacement_rot[:,:,1] )
mesh.attach_field('displacement_n', displacement_rot[:,:,2] )
mesh.write_h5('result.h5')

# +
tol = np.radians(1.)
long = 0
#mask = (sph_crd[:,:,2] > (R_Earth - tol)) * (lon_ox < (np.radians(long)+tol) ) * (lon_ox > (np.radians(long)-tol) )
mask = (sph_crd[:,:,2] > (R_Earth - tol)) * (sph_crd[:,:,1] < (np.radians(long)+tol) ) * (sph_crd[:,:,1] > (np.radians(long)-tol) )
#lt = lat_ox[mask]
lt = np.pi - sph_crd[:,:,0][mask]
indx = np.argsort(lt)

components = ['vertical', 'north','east']

fig, ax = plt.subplots(1,3, figsize = (25,9))
for i,x in enumerate(ax):
    disp = displacement_rot[:,:,i][mask]
    print(np.max(abs(disp)))
    x.semilogx(np.degrees(lt[indx]) , disp[indx], '-')
    x.set_title(components[i])
    x.set_xlabel(f'180 - longitudes, deg')
    x.grid(visible=True, which='both', axis='both')
    x.set_xticks(np.arange(90,271,10))
    x.set_xticklabels(np.arange(0,181,10))
# -

fig, ax = plt.subplots(1,3, figsize=(50,10), subplot_kw={"projection":'aitoff'})
for i, comp in enumerate(components):
    im = ax[i].scatter(sph_crd[:,:,1][mask], sph_crd[:,:,0][mask],  c=np.log10(np.abs(displacement_rot[:,:,i][mask])), s=1., cmap='gist_ncar')
    plt.colorbar(im, ax=ax[i], label=f'log10 u_{comp}', )
    ax[i].set( title=comp,)

# **Calculating spherical harmonics coefficients for the Earth surface displacements**

# +
mask = (sph_crd[:,:,2] > (R_Earth - tol)) 

order = 40
shexpans = np.zeros((2, order+1, order+1, dim))
for i in range(dim):
    shexpans[:,:,:,i], misfit = pysh.shtools.SHExpandLSQ(displacement_rot[:,:,i][mask], np.degrees(sph_crd[:,:,0][mask]), np.degrees(sph_crd[:,:,1][mask]), lmax=order)
# -

# **Plotting the spherical harmonic expansion**

plt.plot(shexpans[0, :,0, 0])
plt.title("C_l(m=0) expansion coefficients")

fig, ax = plt.subplots(dim,1, figsize = (15,18))
for i in range(dim):
    ls = np.linspace(0,180,200)
    sph_harm_exp = sph_harm_m0(shexpans[0, :,0, i])
    displ = [sph_harm_exp.subs('coslat', np.cos(np.radians(lat))) for lat in ls]
    ax[i].plot( ls, displ, '-')
    ax[i].set_title(components[i])
    ax[i].set_xlabel(f'90 - latitude, deg')
    ax[i].grid(visible=True, which='both', axis='both')
    ax[i].set_xticks(np.arange(0,181,10))
    ax[i].set_xticklabels(np.arange(90,-91,-10))

from sympy import legendre, Symbol, assoc_legendre, sin
def sph_harm_m0(coefs):
    expr = 0
    theta = Symbol('theta')
    N = coefs.shape[0]
    for l in range(N):
            expr += coefs[l] * assoc_legendre(l,0, sin(theta)) 
    return expr


expr = sph_harm_m0(shexpans[0, :,0, 0])

mask = (sph_crd[:,:,2] > (R_Earth - tol)) 
reconstruction = np.array([expr.subs({'theta': np.radians(theta)}) for theta in sph_crd[:,:,0][mask]], dtype=np.float64)

fig, ax = plt.subplots(2,1, figsize=(10,10), subplot_kw={"projection":'aitoff'})
i=1
im = ax[i].scatter(sph_crd[:,:,1][mask], sph_crd[:,:,0][mask],  c=np.log10(np.abs(reconstruction)), s=1., cmap='gist_ncar')
plt.colorbar(im, ax=ax[i], label=f'log10 u_{comp}', )
ax[i].set( title=comp,)

fig, ax = plt.subplots(1,3, figsize=(50,10), subplot_kw={"projection":'aitoff'})
for i, comp in enumerate(components):
    im = ax[i].scatter(sph_crd[:,:,1][mask], sph_crd[:,:,0][mask],  c=np.log10(np.abs(displacement_rot[:,:,i][mask])), s=1., cmap='gist_ncar')
    plt.colorbar(im, ax=ax[i], label=f'log10 u_{comp}', )
    ax[i].set( title=comp,)
