# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.8
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

# +
import numpy as np
import os
import sys; sys.path.insert(1, './classes/')
import matplotlib.pyplot as plt; plt.style.use('ggplot')
import time

from elastostatic import *
from tools import *

tol = 1e-10
components = ['V', 'N', 'E']
nature = ['Re', 'Im']
# -

np.degrees(np.arctan2( -0,-1))

x = -124
t = 564
a = (x if x > 10 else t)

a


# +
def open_simple_grid(filename = '../stations/LandOceanGrid_x_2.0_y_2.0_bounds_-89_89_0_360.txt'):
    with open(filename, 'r') as file:
        return np.array([line.split() for line in file.readlines()], dtype=float)
    
def polar2compl(magn, phase):
    r = magn*np.cos(phase)
    i = magn*np.sin(phase)
    return r, i

def compl2polar(real, imag):
    absv  = np.linalg.norm([real, imag])
    phase = np.arctan2(imag, real)
    phase = phase if imag > 0 else 2*np.pi + phase
    return absv, phase


# -

regular_grid = open_simple_grid()

indices = [str(ind) for ind in regular_grid[:,2].astype(int)]
side_points = get_gps_stations(indeces = indices, latitudes = regular_grid[:,0], longitudes = regular_grid[:,1],)

receivers = {f'{station.name}':{'SidePoint': station} for station in side_points}

# +
with open("../stations/cn_OceanOnly_GOT410c-M2_cm_convgf_GOT410c_global_PREM.txt") as file:
    data = np.array([line.split() for line in file.readlines()])
header = data[0]
regular_grid = data[1:].astype(float)

indices     = [str(ind) for ind in regular_grid[:,0].astype(int)]
east_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,3], regular_grid[:,4])])/1000.
north_ri    = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,5], regular_grid[:,6])])/1000.
vert_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,7], regular_grid[:,8])])/1000.
side_points = get_gps_stations(indeces = indices, latitudes = regular_grid[:,1], longitudes = regular_grid[:,2],)
# -

for i, ind in enumerate(regular_grid[:,0].astype(int)):
    receivers[f'XX.{ind}.']['lat'] = regular_grid[i,1]
    receivers[f'XX.{ind}.']['lon'] = regular_grid[i,2]

receivers = {f'{station.name}':{'SidePoint': station, 
                                'lat': lat, 'lon': lon,   
                                'E-Re-LD': e_re, 'E-Im-LD': e_im, 
                                'N-Re-LD': n_re, 'N-Im-LD': n_im, 
                                'V-Re-LD': v_re, 'V-Im-LD': v_im,
                                'E-Ab-LD': e_ab, 'E-Ph-LD': e_ph,
                                'N-Ab-LD': n_ab, 'N-Ph-LD': n_ph,
                                'V-Ab-LD': v_ab, 'V-Ph-LD': v_ph} 
             for station,
                 lat, lon,
                 e_re, e_im, 
                 n_re, n_im, 
                 v_re, v_im,
                 e_ab, e_ph,
                 n_ab, n_ph,
                 v_ab, v_ph
             in 
             zip(side_points,
                 regular_grid[:,1], regular_grid[:,2],
                 east_ri [:,0], east_ri [:,1], 
                 north_ri[:,0], north_ri[:,1], 
                 vert_ri [:,0], vert_ri [:,1],
                 regular_grid[:,3], regular_grid[:,4],
                 regular_grid[:,5], regular_grid[:,6],
                 regular_grid[:,7], regular_grid[:,8])
            }




for i, ind in enumerate(indices):
    for j, comp in enumerate(components):
        ab_ld, ph_ld = compl2polar(receivers[f'XX.{ind}.'][f'{comp}-Re-LD'], receivers[f'XX.{ind}.'][f'{comp}-Im-LD'])
        receivers[f'XX.{ind}.'][f'{comp}-Ph-LD-test'] = np.degrees(ph_ld)



# +
fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
    for j, nat in enumerate(['Ab', 'Ph']):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-{nat}-LD'] for ind in indices]
        im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[j, i], label=f'Loaddef_{nat}_{comp}')

# +
fig, ax = plt.subplots(1,3, figsize=(55,10), subplot_kw={"projection":'aitoff'})

lat = [receivers[f'XX.{ind}.']['lat'] for ind in indices]
lon = [receivers[f'XX.{ind}.']['lon']-180 for ind in indices]
for i, comp in enumerate(components):
        field_to_plot = [receivers[f'XX.{ind}.'][f'{comp}-Ph-LD-test'] + 180 for ind in indices]
        im = ax[i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
        plt.colorbar(im, ax=ax[i], label=f'Loaddef_{nat}_{comp}')
# -
ind = np.where(np.array([(receivers[f'XX.{ind}.'][f'{comp}-Ph-LD-test']) - receivers[f'XX.{ind}.'][f'{comp}-{nat}-LD'] for ind in indices]) < -300)


np.array([receivers[f'XX.{ind}.'][f'{comp}-Ph-LD-test'] for ind in indices])[ind[0]]

ind[0]


