# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.13.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

# +
import numpy as np
import os
import sys; sys.path.insert(1, './classes/')
import matplotlib.pyplot as plt; plt.style.use('ggplot')
import time

from elastostatic import *

# +
a = elastostatic_solver(meshfile = "../meshes/elastostatic/mesh.h5")
a.config.run.tensor_order=2
a.construct_mesh(with_surface_topo = False, with_moho_topo = False, 
                 nex = 30, buffer = 0, oneD_model="prem_iso_one_crust",
                 local_refinement_level=0, global_refinement_level=0)

a.prepare_mesh_fields(real_or_imag = "re")
# -

mass_matrix = get_mass_matrix(a.mesh)
volume_p = np.sum(mass_matrix)
mass_p = np.sum(mass_matrix * a.mesh.element_nodal_fields['RHO'])
print("Model volume:    {:>.3g}".format(volume_p))
print("Model mass:      {:>.5g}".format(mass_p))
print("Average density: {:>.6g}".format(mass_p/volume_p))

# **!!! Issue with the interpolations of the nodal and elemental fields.**

# **Refining the mesh around the centre**

reference_radius = 200000.
mask = np.zeros(a.mesh.nelem)
mask[a.mesh.get_element_centroid_radius() < reference_radius ] = 1
output = a.mesh.refine_locally(mask.astype(bool), reinterpolate_nodal_fields = True)
a.find_boundary_centre(tolerance=200000.)

# **Reassigning the density arrays**

# +
from salvus.mesh.models_1D import model 

m = model.built_in(a.oneD_model)

relradii = np.linalg.norm(a.mesh.points, axis = -1)/a.config.const.R_Earth
relradii[relradii > 1.0] = 1.0

RHO = np.array([m.get_rho(r) for r in relradii])[a.mesh.connectivity]
# -

# **Verifying the total mass**

mass_matrix = get_mass_matrix(a.mesh)
volume_a    = np.sum(mass_matrix)
mass_a      = np.sum(mass_matrix * RHO)
print("Model volume:    {:>.3g}".format(volume_a))
print("Model mass:      {:>.5g}".format(mass_a))
print("Average density: {:>.6g}".format(mass_a/volume_a))

# **!!! Issue with the mass:**

# The volume change leads to the mass change?

print("Volumetric mass anomaly effect: {:>.3g}".format( abs(volume_p - volume_a) * mass_p/volume_p))
print("Relative volume anomaly:        {:>.6g} ".format( abs(volume_p - volume_a) / volume_p) ) 

print("Observed mass anomaly: {:>.3g}".format(abs(mass_p - mass_a)))
print("Relative mass anomaly: {:>.4g}".format(abs(mass_p - mass_a)/mass_p))

# **How to reassign all other properties?**
