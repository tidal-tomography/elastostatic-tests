# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %load_ext autoreload

# %autoreload 2

# +
import numpy as np
import os
import sys; sys.path.insert(1, './classes/')

import matplotlib.pyplot as plt; plt.style.use('ggplot')
import matplotlib
font = {'family' : 'sans',
        'weight' : 'bold',
        'size'   : 16}
matplotlib.rc('font', **font)

import time
import pyshtools

from elastostatic import *
from tools        import *

import salvus.namespace as sn

def setup_folders(folders):
    for folder in folders:
        os.makedirs(os.path.dirname(folder), exist_ok=True)


# +
tol = 1e-10
components = ['V', 'N', 'E']
nature = ['Re', 'Im']
harmonics = ['M2', 'O1']

##################################
harmonic         = 'M2'  #########
##################################
load_models_dir = '../input/ocean_load_model/'
if   harmonic == 'O1': 
    load_models_file = f'{load_models_dir}tidal_load_o1_256_seamask.nc'
elif harmonic == 'M2': 
    load_models_file = f"{load_models_dir}tidal_load_m2_10800_new_grad_new_mask.nc"

loaddef_dir     = f'../input/stations/{harmonic}/'
loaddef_file    = f"{loaddef_dir}cn_OceanOnly_GOT410c-{harmonic}_cm_convgf_GOT410c_global_PREM.txt"

mesh_dir        = f'../output/meshes/elastostatic/{harmonic}/'
pictures_dir    = f'../output/pics/{harmonic}/'
solution_dir    = f'../output/solution/elastostatic/{harmonic}/'

setup_folders([mesh_dir, pictures_dir, solution_dir])

###################################
create_mesh      = False
LMAX             = 2048
TENSOR_ORDER     = 2
REFINEMENT_LEVEL = 2
NEX              = 24

run_sim       = True
SITE          = 'daint'
RANKS         = 120
REL_TOLERANCE = 1e-10

volumetric_output = False
visualize         = True #add functionality
###################################

# +
def setup_mesh(meshfile = f"{mesh_dir}mesh.h5"):
    a = elastostatic_solver(meshfile)
    a.config.input_files.tidal_loading_lmax_1 = LMAX
    a.config.input_files.ocean_tides_model = load_models_file
    
    a.config.run.tensor_order = TENSOR_ORDER
    a.construct_mesh(with_surface_topo = False, 
                     with_moho_topo    = False, 
                     oneD_model        = "prem_iso_one_crust",
                     nex               = NEX,
                     local_refinement_level=REFINEMENT_LEVEL)
    
    def dist(x):
        oned_stuff = oneD(a.oneD_model, a.config.const.R_Earth)
    #return np.abs(1221501. - np.linalg.norm(x, axis=-1)) #inner core boundary
        return np.abs(oned_stuff.cmb_depth - np.linalg.norm(x, axis=-1)) 
    a.mesh.find_side_sets_generic("r0", dist, tolerance=10.)
    return a

def read_mesh(meshfile = f"{mesh_dir}mesh.h5"):
    a = elastostatic_solver(meshfile)
    a.mesh = sn.UnstructuredMesh.from_h5(meshfile)
    return a

def run_simulation(nat, side_points):
    assert nat in ['re', 'im'], "nat needs to be re or im"
    a.prepare_mesh_fields(real_or_imag = nat)

    a.config.elastos_run.relative_tolerance = REL_TOLERANCE
    a.config.run.ranks                      = RANKS
    a.config.run.wall_time_in_seconds       = 3600
    a.config.elastos_run.max_iterations     = 6000
    a.config.elastos_run.point_solution     = a.config.elastos_run.solution_folder + f"receivers_{nat}.h5"
    a.config.elastos_run.volume_solution    = a.config.elastos_run.solution_folder +  f"solution_{nat}.h5"
    a.config.run.site_name                  = SITE
    sim_re                                  = a.run_simulation(stations = side_points)
    
    a.mesh.attach_field(f'NEUMANN_{nat}', a.mesh.elemental_fields['NEUMANN'])
    
def plot_misfit_evolution(filename = f'{solution_dir}stdout'):
    with open(filename) as file:
        data      = np.array(file.readlines())
    begin     = np.where(data == '----- BEGIN ITERATION LOG -----\n')[0][0] + 5
    end       = np.where(data == '----- END ITERATION LOG -----\n')[0][0]
    data      = data[begin:end]
    rel_error = np.array([line.split() for line in data], dtype=np.float64)
    plt.figure(figsize=(20, 4))
    plt.semilogy(rel_error[:,0], rel_error[:,2])
    plt.xlabel('Iteration')
    plt.ylabel('Rel.error^2')
    print('Minimal rel.error^2:', np.min(rel_error))
    
def get_receiver_array(loaddef_file=loaddef_file):
    with open(loaddef_file) as file:
        data = np.array([line.split() for line in file.readlines()])
    header = data[0]
    regular_grid = data[1:].astype(float)

    indices = [str(ind) for ind in regular_grid[:,0].astype(int)]
    side_points = get_gps_stations(indeces = indices, latitudes = regular_grid[:,1], longitudes = regular_grid[:,2],)

    east_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,3], regular_grid[:,4])])
    north_ri    = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,5], regular_grid[:,6])])
    vert_ri     = np.array([polar2compl(magn, np.radians(phase)) for magn, phase in zip(regular_grid[:,7], regular_grid[:,8])]) 

    receivers   = {f'{station.name}':{'SidePoint': station, 
                                    'lat': lat, 'lon': lon,   
                                    'E-Re-LD': e_re, 'E-Im-LD': e_im, 
                                    'N-Re-LD': n_re, 'N-Im-LD': n_im, 
                                    'V-Re-LD': v_re, 'V-Im-LD': v_im,
                                    'E-Ab-LD': e_ab, 'E-Ph-LD': e_ph,
                                    'N-Ab-LD': n_ab, 'N-Ph-LD': n_ph,
                                    'V-Ab-LD': v_ab, 'V-Ph-LD': v_ph} 
                 for station,
                     lat, lon,
                     e_re, e_im, 
                     n_re, n_im, 
                     v_re, v_im,
                     e_ab, e_ph,
                     n_ab, n_ph,
                     v_ab, v_ph
                 in 
                 zip(side_points,
                     regular_grid[:,1], regular_grid[:,2],
                     east_ri [:,0], east_ri [:,1], 
                     north_ri[:,0], north_ri[:,1], 
                     vert_ri [:,0], vert_ri [:,1],
                     regular_grid[:,3], regular_grid[:,4],
                     regular_grid[:,5], regular_grid[:,6],
                     regular_grid[:,7], regular_grid[:,8])
                }
    
    return side_points, receivers

def plot_results(receivers, code, nature = ['Re', 'Im']):
    assert code in ['SLV', 'LD', 'Misfit']
    indices = list(receivers.keys())
    lat = [receivers[ind]['lat'] for ind in indices]
    lon = [receivers[ind]['lon']-180 for ind in indices]
    
    fig, ax = plt.subplots(2,3,figsize=(55,15), subplot_kw={"projection":'aitoff'})
    
    for i, comp in enumerate(components):
        for j, nat in enumerate(nature):
            field_to_plot = [receivers[ind][f'{comp}-{nat}-{code}'] for ind in indices]
            im = ax[j, i].scatter( np.radians(lon), np.radians(lat), c=field_to_plot)
            plt.colorbar(im, ax=ax[j, i], label=f'{code}_{nat}_{comp}')
    plt.savefig(f'{pictures_dir}_{code}_{"".join(nature)}_{harmonic}.png')
    
def output_stations(receivers, filename=f'../output/stations/stations_solution_{harmonic}.txt'):
    components = ['E', 'N', 'V']
    nature     = ['Ab', 'Im']
    with open(filename, 'w') as file:
        file.write(f"Station  Lat(+N,deg)  Lon(+E,deg)")
        for comp in components: 
            for nat in nature: 
                file.write(f"      {comp}-{nat}(mm)  ")
                
        for ind in list(receivers.keys()):
            file.write(f"\n{int(receivers[ind]['SidePoint'].name.split('.')[1]):8} {receivers[ind]['lat']:10}, {receivers[ind]['lon']:10} ")
            for comp in components: 
                for nat in nature: 
                    file.write(f"{receivers[ind][f'{comp}-{nat}-SLV']:15.8f} ")


# -

# **Setup mesh**

# +
side_points, receivers = get_receiver_array()

if create_mesh:
    a = setup_mesh()
else:
    a = read_mesh()
    
sph_crd = get_sph_coord(a.mesh)
a.config.elastos_run.solution_folder = solution_dir
a.config.elastos_run.update()
# -

# **Running real and imaginary loading simulations**

if run_sim:
    #assert len(ssh_keys_from_agent()) > 0
    for nat in nature:
        prepare_mesh_fields(a.mesh, nat.lower())
        a.write_mesh(path = a.meshfile)
        run_simulation(nat.lower(), side_points)
        plot_misfit_evolution(filename = a.config.elastos_run.solution_folder + 'stdout')
        plt.savefig(f'{pictures_dir}convergence_{nat}_{harmonic}.png')

# **Reading the volumetric solution, rotate it and add to the mesh**

# +
displacement     = np.zeros((2, a.mesh.nelem, a.mesh.nodes_per_element, a.mesh.ndim))
displacement_rot = np.zeros((2, a.mesh.nelem, a.mesh.nodes_per_element, a.mesh.ndim))

#Reading the volumetric solutions and change the CS
if volumetric_output:
    for i, nat in enumerate(nature):
        a.config.elastos_run.volume_solution = f"{a.config.elastos_run.solution_folder}solution_{nat.lower()}.h5"
        displacement[i] = read_volumetric_results(a.config.elastos_run.volume_solution)

        shape = displacement[i].shape
        displ_temp = displacement[i].reshape((shape[0]*shape[1], 3))
        lat_flat   = sph_crd[:,:,0] .reshape((shape[0]*shape[1]))
        lon_flat   = sph_crd[:,:,1] .reshape((shape[0]*shape[1]))
        displacement_rot[i] = np.array([get_rotation_matrix(lat, lon).dot(vec) for lat,lon,vec in zip(lat_flat, lon_flat, displ_temp)]).reshape(shape)
        
        for j, comp in enumerate(components):
            a.mesh.attach_field(f'displacement_{comp}_{nat}', displacement_rot[i, :,:,j] )

#Adding phase and amplitude fields to the mesh
if volumetric_output:
    displacement_cpx = np.zeros_like(displacement_rot[0],dtype=np.complex64)
    displacement_cpx.real = displacement_rot[0]
    displacement_cpx.imag = displacement_rot[1]
    for i, comp in enumerate(components):
        a.mesh.attach_field(f'disp_absolute_{comp}' , np.absolute(displacement_cpx[:,:,i]))
        a.mesh.attach_field(f'disp_phase_{comp}'   , np.angle(displacement_cpx[:,:,i]))
    a.mesh.write_h5(f'{solution_dir}result.h5')
# -

# **Reading the point solution**


for k, nat in enumerate(nature):
    filename = f"{a.config.elastos_run.solution_folder}receivers_{nat.lower()}.h5"
    indices_op, results = read_receiver_results(filename)
    for i, ind in enumerate(indices_op):
        for j, comp in enumerate(components):
            #reading point displacements
            receivers[ind.decode()][f'{comp}-{nat}-SLV'] = results[i, j] *1000.
            #compute misfit
            receivers[ind.decode()][f'{comp}-{nat}-Misfit'] = (receivers[ind.decode()][f'{comp}-{nat}-SLV'] 
                                                                 - receivers[ind.decode()][f'{comp}-{nat}-LD'])
#get the point displacements in polar coords
for i, ind in enumerate(indices_op):
    for j, comp in enumerate(components):
        ab_slv, ph_slv  = compl2polar(receivers[ind.decode()][f'{comp}-Re-SLV'], receivers[ind.decode()][f'{comp}-Im-SLV'])
        ab_ld , ph_ld   = compl2polar(receivers[ind.decode()][f'{comp}-Re-LD'] , receivers[ind.decode()][f'{comp}-Im-LD'])
        receivers[ind.decode()][f'{comp}-Ab-SLV'] = ab_slv 
        receivers[ind.decode()][f'{comp}-Ph-SLV'] = np.degrees(ph_slv)
        receivers[ind.decode()][f'{comp}-Ab-LD']  = ab_ld  
        receivers[ind.decode()][f'{comp}-Ph-LD']  = np.degrees(ph_ld) 

output_stations(receivers)

plot_results(receivers, 'SLV')

plot_results(receivers, 'LD')

plot_results(receivers, 'Misfit')

plot_results(receivers, 'SLV', nature=['Ab', 'Ph'])

plot_results(receivers, 'LD', nature=['Ab', 'Ph'])


