from salvus.mesh import UnstructuredMesh
from salvus.flow import simple_config
import salvus.namespace as sn

import h5py
import numpy as np
from pympler import asizeof

import paramiko

def get_mesh_dims(mesh):
    assert type(mesh) == UnstructuredMesh
    return (mesh.nelem, mesh.nodes_per_element)
    
def cart2geogr(coords):
    longitude = np.arctan2(coords[1], coords[0])
    latitude  = np.arctan2(coords[2], np.linalg.norm(coords[0:2], axis=-1))
    radius    = np.linalg.norm(coords, axis=-1)
    return [latitude, longitude, radius]

def get_sph_coord(mesh):
    coords     = mesh.points.copy()
    longitudes = np.arctan2(coords[:,1], coords[:,0])
    latitudes  = np.arctan2(coords[:,2], np.linalg.norm(coords[:,0:2], axis=-1))
    radii      = np.linalg.norm(coords, axis=-1)
    sphcrd     = np.zeros((mesh.nelem, mesh.nodes_per_element, mesh.ndim))
    sphcrd[:,:,0] = latitudes [mesh.connectivity]
    sphcrd[:,:,1] = longitudes[mesh.connectivity]
    sphcrd[:,:,2] = radii     [mesh.connectivity]
    return sphcrd

def get_sideset_mask(mesh, sideset):
        assert type(mesh) == UnstructuredMesh
        assert sideset in mesh.side_sets.keys()
        facets = { 'to=2':
                     {'0':  list(range(  0,   9)),
                      '1':  list(range( 18,  27)),
                      '2':  list(range(  0,   3)) + \
                            list(range(  9,  12)) + \
                            list(range( 18,  21)),
                      '3':  list(range(  6,   9)) + \
                            list(range( 15,  18)) + \
                            list(range( 24,  27)),
                      '4':  list(range(2, 27, 3)),
                      '5':  list(range(0, 27, 3)),},

                   'to=4':
                     {'0':  list(range(0,25)),
                      '1':  list(range(100,125)),
                      '2':  list(range(  0,   5)) + \
                            list(range( 25,  30)) + \
                            list(range( 50,  55)) + \
                            list(range( 75,  80)) + \
                            list(range(100, 105)),
                      '3':  list(range( 20,  25)) + \
                            list(range( 45,  50)) + \
                            list(range( 70,  75)) + \
                            list(range( 95, 100)) + \
                            list(range(120, 125)),
                      '4':  list(range(4,125,5)),
                      '5':  list(range(0,125,5)),}
             }

        tensor_order = int(np.rint((mesh.nodes_per_element)**(1/3.)) - 1) #it's redundant but clearer
        assert tensor_order in set([2,4])
        n_nodes_per_element = (tensor_order+1)**3
        n_nodes_facet       = (tensor_order+1)**2
        print(mesh.nodes_per_element, tensor_order)
        elements = mesh.side_sets[sideset][0]
        edges    = mesh.side_sets[sideset][1]
        mask = (np.repeat(elements, n_nodes_facet), np.array([facets[f'to={tensor_order}'][str(edge)] for edge in edges]).ravel())
        return mask

def objectsize(obj):
    #obj size in bytes:
    size = asizeof.asizeof(obj) 
    count = 0
    units = ['b', 'Kb', 'Mb', 'Gb']
    while (size >= 1024.):
        size /= 1024.
        count += 1
    return f'{size:.2f} {units[count]}'


def get_rotation_matrix(lat, lon):
    return np.array([[ np.cos(lat)*np.cos(lon),  np.cos(lat)*np.sin(lon), np.sin(lat)],
                     [-np.sin(lat)*np.cos(lon), -np.sin(lat)*np.sin(lon), np.cos(lat)],
                     [-np.sin(lon)            ,              np.cos(lon),           0]])

def get_gps_stations_from_csv(path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
    with open(path) as file:
        header   = file.readline()
        stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

    receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                         side_set_name='r1', station_code=name, fields=["displacement"]) 
                                         for name, lat, lon in zip(stations.index, stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
    return receivers

def get_gps_stations(indeces, latitudes, longitudes,):
        return [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(indeces, latitudes, longitudes) ]

def read_receiver_results(filename):
    with h5py.File(filename) as file:
        data    = np.array(file['point']['displacement'])
        indices = file['names_ELASTIC_point'][:]
    return indices, data[:, :, -1]

def read_volumetric_results(filename, field_name_prefix = "solution"):
    solution = sn.UnstructuredMesh.from_h5(filename)
    displacement = np.zeros((solution.nelem, solution.nodes_per_element, solution.ndim))
    for i, comp in enumerate(['_X', '_Y', '_Z']):
        displacement[:,:,i] = solution.element_nodal_fields[field_name_prefix + comp]
    return displacement


def ssh_keys_from_agent():
    agent = paramiko.agent.Agent()
    return agent.get_keys()

def test_daint_connection():
    keys = ssh_keys_from_agent()

    host = "daint.cscs.ch"
    special_account = "admitrov"
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    
    proxy = paramiko.ProxyCommand("ssh -Y admitrov@ela.cscs.ch netcat daint.cscs.ch 22 -w 10")
    try:
        client.connect(host, username=special_account, pkey=keys[1], sock = proxy)
        _stdin, stdout, _stderr = client.exec_command("echo 'test'")
        lines = stdout.read().decode()
        if lines == 'test':
            print('Connection with daint was successfully established')
    except:
        print('Connection with daint was not established')
        
    client.close()
    
def polar2compl(magn, phase):
    r = magn*np.cos(phase)
    i = magn*np.sin(phase)
    return r, i

def compl2polar(real, imag):
    absv  = np.linalg.norm([real, imag])
    phase = np.arctan2(imag, real)
    phase = phase if imag > 0 else 2*np.pi + phase
    return absv, phase

def prepare_mesh_fields(mesh, real_or_imag ='re', rho_water=1030.):
        keys = set(mesh.elemental_fields.keys())
        assert (real_or_imag in ['re', 'im'])
        for field in [f'tidal_elevation_{real_or_imag}', 'g']:
            assert field in keys, f'field \'{field}\' not attached'
        mesh.attach_field('fluid', np.zeros(mesh.nelem))
        mesh.attach_field('zeros', np.zeros((mesh.nelem, mesh.nodes_per_element)))
        mesh.map_nodal_fields_to_element_nodal()
        
        if real_or_imag == "re":
            NEUMANN = -1 * rho_water * mesh.elemental_fields[f'tidal_elevation_{real_or_imag}'] * mesh.elemental_fields['g']
            
        elif real_or_imag == "im":
            #imaginary part has a pastward imaginary part - the sign has to be swapped
            mesh.elemental_fields['tidal_elevation_'+real_or_imag] *= (-1)
            NEUMANN = -1 * rho_water * mesh.elemental_fields[f'tidal_elevation_{real_or_imag}'] * mesh.elemental_fields['g']
        mesh.attach_field('NEUMANN', NEUMANN)