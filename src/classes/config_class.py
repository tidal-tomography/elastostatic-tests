class const:
    def __init__(self): 
        self.G       = 6.67430e-11
        self.R_Earth = 6371.* 1e3
        self.i_core  = 1220.* 1e3
        self.cmb     = 3481.* 1e3

class input_files:
    def __init__(self):
        self.ocean_tides_model  = "../ocean_load_model/tidal_load_m2_10800_new_grad_new_mask.nc"
        self.surface_topography = "../topography files/topography_earth2014_egm2008_lmax_256.nc"
        self.moho_topography    = "../topography files/moho_topography_crust_1_0_egm2008.nc"
        
class run:
    def __init__(self):
        self.tensor_order         = 4
        self.ranks                = 10
        self.wall_time_in_seconds = 1000
        self.site_name            = "local"
        
class gravity_run:
    def __init__(self):
        self.boundary_condition = "dirichlet"
        self.solution_folder = "../solutions/gravity/"
        self.volume_solution = self.solution_folder + "solution.h5"
        self.volume_solution_fields = ["solution", "residuals", "right-hand-side"]
        self.point_solution  = self.solution_folder + "receivers.h5"
        self.point_solution_fields = ["phi_t"]
        
        self.tidal_loading_lmax_1 = 256
        self.tidal_loading_lmax_2 = 256 
        
        #CG parameters:
        self.max_iterations     = 3000
        self.absolute_tolerance = 0.0
        self.relative_tolerance = 10e-10
        self.preconditioner     = True
    def update(self):
        self.volume_solution = self.solution_folder + "solution.h5"
        self.point_solution  = self.solution_folder + "receivers.h5"
        
class elastos_run:
    def __init__(self):
        self.boundary_condition = "dirichlet"
        self.solution_folder = "../solutions/elastostatic/"
        self.volume_solution = self.solution_folder + "solution.h5"
        self.volume_solution_fields = ["solution", "residuals", "right-hand-side"]
        self.point_solution  = self.solution_folder + "receivers.h5"
        self.point_solution_fields = ["deformation"]

        #CG parameters:
        self.max_iterations     = 10000
        self.absolute_tolerance = 0.0
        self.relative_tolerance = 10e-10
        self.preconditioner     = True
    def update(self):
        self.volume_solution = self.solution_folder + "solution.h5"
        self.point_solution  = self.solution_folder + "receivers.h5"


