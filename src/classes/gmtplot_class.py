import salvus
import xarray as xr
import pygmt
from salvus.mesh.unstructured_mesh_utils import extract_model_to_regular_grid
import numpy as np
import scipy

class gmtplot:
    def __init__(self, mesh, field, resolution=101):
        self.mesh  = mesh
        self.field = field
        self.res   = resolution
    
    def surface_nodal_indeces(self):
        m=self.mesh
        if 'external' in m.elemental_fields.keys():
            loc_indeces = np.where(m.elemental_fields['external'][m.side_sets['r1'][0]] == 0)
        else:
            loc_indeces = np.where(m.elemental_fields['layer'][m.side_sets['r1'][0]] < 100) #condition that always works
        gl_indeces = (m.side_sets['r1'][0][loc_indeces], m.side_sets['r1'][1][loc_indeces])
        gl_indeces_full = (np.zeros((gl_indeces[0].shape[0]*25,),dtype=int ), 
                           np.zeros((gl_indeces[0].shape[0]*25,),dtype=int ))
        n = 25
        facets = {'0':list(range(0,25)),
                  '1':list(range(100,125)),
                  '2':[0, 1, 2, 3, 4,
                       25, 26, 27, 28, 29,
                       50, 51, 52, 53, 54,
                       75, 76, 77, 78, 79,
                       100, 101, 102, 103, 104],
                  '3':[20, 21, 22, 23, 24,
                       45, 46, 47, 48, 49,
                       70, 71, 72, 73, 74,
                       95, 96, 97, 98, 99,
                       120, 121, 122, 123, 124],
                  '4':list(range(4,125,5)),
                  '5':list(range(0,125,5)),}
        for i in range(gl_indeces[0].shape[0]):
            k = i*25
            gl_indeces_full[1][k:k+25] = facets[str(gl_indeces[1][i])]
            gl_indeces_full[0][k:k+25] = [gl_indeces[0][i]]*25
        m.global_indeces = gl_indeces_full
        return gl_indeces_full

    def extract_data_from_mesh(self):
        m = self.mesh
        field = self.field
        global_indeces = m.global_indeces
        assert field in m.elemental_fields
        self.z = m.points[m.connectivity][global_indeces][:,2]
        self.x = m.points[m.connectivity][global_indeces][:,0]
        self.y = m.points[m.connectivity][global_indeces][:,1]
        self.data = m.elemental_fields[field][global_indeces][:]

    def get_geographical_coords(self):
        x,y,z = self.x, self.y, self.z
        lat   = np.degrees(np.arctan(z/np.sqrt(y**2 + x**2))) 
        lon   = np.degrees(np.arctan(y/x)) 
        lon[(x < 0.)*(y >= 0.)] = 180 + lon[(x < 0.)*(y >= 0.)]
        lon[(x < 0.)*(y <= 0.)] -= 180. 
        self.lon = lon
        self.lat = lat
        return lon, lat

    def interpolate_irregular_grid(self):
        n = self.res
        lon = self.lon
        lat = self.lat
        data = self.data
        lon_inter, lat_inter = np.meshgrid(np.linspace(np.min(lon), np.max(lon), n), 
                                           np.linspace(np.min(lat), np.max(lat), n))
        a = np.array([lon, lat]).T
        interp_result = scipy.interpolate.griddata(a, values = data, xi=(lon_inter.ravel(), lat_inter.ravel()), 
                                            fill_value=np.mean(data))
        self.lon_inter = lon_inter
        self.lat_inter = lat_inter
        self.field_inter = interp_result
        return lon_inter, lat_inter, interp_result

    def make_dataarray(self):
        n      = self.res
        result = self.field_inter
        self.da =  xr.DataArray(data = result.reshape(n,n),
                            dims = ['lat', 'lon'],
                            coords = dict(
                                lat = (np.linspace(-90, 90, n)),
                                lon = (np.linspace(-180, 180, n))
                          ))
        return self.da

    def pygmt_plot(self,  name, colorbar_name, limits=[],cmap="haxby", save=False):
        da =self.da
        fig = pygmt.Figure()
        fig.basemap(region='d', projection="R20c", frame=["a",f'+t"{name}"'])
        limits = [float(np.min(da)), float(np.max(da))]
        if len(limits) != 2: limits = [float(np.min(da)), float(np.max(da))]
        if limits[0] >= limits[1]: limits[0] -= 1e-5 * np.min(np.abs(limits))
        pygmt.makecpt(cmap=cmap, series=limits)
        fig.grdimage(grid=da)
        fig.coast(shorelines="0.5p,black")
        colorbar_label = f"x+l{colorbar_name}"
        fig.colorbar(frame=["a", colorbar_label])
        self.fig = fig
        if save: fig.savefig(f'{name}.pdf')
        return fig