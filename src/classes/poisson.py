from .octiload_class import *
from salvus.mesh.unstructured_mesh_utils import extract_model_to_regular_grid
import xarray as xr

class poisson_solver(octiload):

    def __init__(self, meshfile = '../meshes/poisson/mesh.h5'):
        self.config   = config()
        self.meshfile = meshfile
        if not os.path.exists(os.path.dirname(meshfile)): os.makedirs(os.path.dirname(meshfile))
            
    def add_oneD_solution(self, npoints = 500):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        max_radius = max(np.linalg.norm(self.mesh.points, axis=-1))
        self.max_radius    = max_radius
        self.oneD_solution = oneD(N_points=npoints, max_radius=max_radius, MODEL=self.oneD_model, R_Earth=self.config.const.R_Earth)
    
    def plot_oneD_solution(self, ymax = -100):
        assert hasattr(self, "oneD_solution"),  "There is no 'oneD_solution' in the object. Run 'add_oneD_solution' first"
        fig, ax = plt.subplots(1, 2, sharey=True, figsize=[12,12])
        radii        = self.oneD_solution.radii_relative * self.R_Earth
        potential    = self.oneD_solution.grav_pot
        acceleration = self.oneD_solution.grav_acc
        ax[0].plot(potential, radii)
        ax[0].hlines(self.R_Earth,  xmin = np.min(potential), xmax = np.max(potential), 
                     color='k', linestyle='--', linewidth=3.,)
        ax[1].plot(acceleration, radii)
        ax[1].hlines(self.R_Earth, xmin = np.min(acceleration), xmax = np.max(acceleration), 
                     color='k', linestyle='--', linewidth=3.,)
        
        if ymax > 0:
            for a in ax: a.set_ylim(0, ymax)
        
        ax[0].set_xlabel('grav potential')
        ax[1].set_xlabel('grav acceleration')
        ax[0].set_ylabel('distance from the center of the Earth')
        fig.suptitle("1D solutions")
    
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"

        mesh                                             = self.mesh
        sim                                              = sn.simple_config.simulation.Poisson(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        sim.physics.poisson_equation.mass_matrix_scaling = False
        
        sim.physics.poisson_equation.right_hand_side.filename = self.meshfile
        sim.physics.poisson_equation.right_hand_side.format   = "hdf5"
        sim.physics.poisson_equation.right_hand_side.field    = "RHS"

        sim.physics.poisson_equation.solution.filename        = os.path.basename(self.config.gravity_run.volume_solution)
        sim.physics.poisson_equation.solution.fields          = self.config.gravity_run.volume_solution_fields
            
        bc = self.config.gravity_run.boundary_condition
        if bc == 'dirichlet':
            assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
            boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"], value=-1*float(self.oneD_solution.grav_pot[-1]))
            print("external boundary condition: ", float(self.oneD_solution.grav_pot[-1]))
            sim.add_boundary_conditions(boundaries)
        elif bc == 'dirichlet_zero':
            assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
            boundaries = sn.simple_config.boundary.HomogeneousDirichlet(side_sets=["r2"], value=float(0))
            print("external boundary condition: ", float(0))
            sim.add_boundary_conditions(boundaries)
            
        sim.solver.max_iterations     = self.config.gravity_run.max_iterations
        sim.solver.absolute_tolerance = self.config.gravity_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.gravity_run.relative_tolerance
        sim.solver.preconditioner     = self.config.gravity_run.preconditioner
        
        if type(stations) != bool:
            if hasattr(self, "stations"):
                sim.add_receivers(self.stations) if str(stations) == 'all' else sim.add_receivers(stations)
                sim.output.point_data.format                          = "hdf5"
                sim.output.point_data.filename                        = os.path.basename(self.config.gravity_run.point_solution)
                sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations
        
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.gravity_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds,
        )
        
    def prepare_mesh_fields(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mesh = self.mesh
        
        #adding M0 and M1 elemental field
        M1 = (4 * self.config.const.G * np.pi)**-1
        self.fields           = self.mesh.elemental_fields.keys()
        self.mesh_array_shape = (self.mesh.nelem, self.mesh.nodes_per_element)
        f =  np.ones(self.mesh_array_shape)
        mesh.attach_field('M0', f)
        mesh.attach_field('M1', f * M1)
        
        if ('RHO' in self.fields): mesh.attach_field('RHS', mesh.elemental_fields['RHO'])
        
        #removing all unnessesary fields
        topreserve = ['layer', 'external', 'RHS', 'M0', 'M1']
        for field in list(self.fields):
            if field not in topreserve:
                del mesh.elemental_fields[field]
        
        if ( {'external', 'RHS'}.issubset(set(self.fields)) ):
            mask = np.array(mesh.elemental_fields['external'], dtype = bool)
            mesh.elemental_fields['RHS'][mask] = 0.

        mesh.attach_field('fluid', np.ones(mesh.nelem))
        mesh.map_nodal_fields_to_element_nodal()
        if self.config.gravity_run.boundary_condition == "neumann":
            assert hasattr(self, "oneD_solution"),  "There is no 1D solution in the object. Run 'add_oneD_solution()' first"
            mesh.attach_field('NEUMANN', -1 * self.oneD_solution.grav_acc[-1] * np.ones_like(mesh.elemental_fields['M0']))
        print("Elemental fields in the mesh: ", set(self.mesh.elemental_fields.keys()))
        
    def read_simulation_results(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        solution = sn.UnstructuredMesh.from_h5(self.config.gravity_run.volume_solution)
        self.mesh.attach_field('grav_potential', solution.element_nodal_fields['solution'])
    
    def get_onlyEarth_mesh(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mask  = self.mesh.elemental_fields['external'] < 0.5
        self.earthmesh = self.mesh.apply_element_mask(mask)
    
    def get_analytical_potential_earth(self):
        assert hasattr(self, "earthmesh"),  "There is no mesh in the object. Run 'get_onlyEarth_mesh()' first"
        get_analytical_potential(self.earthmesh, self.MODEL, self.config.const.R_Earth)
        
    def compare_solution_with_analytical(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        if not hasattr(self, "mass_matrix"):
            self.get_mass_matrix()
        #reading simulation results from the folder specified in th econfig object
        self.read_simulation_results()
        #take out only the earth out of the domain
        self.get_onlyEarth_mesh()
        #compute analytical gravitational potential for the earth part of the domain
        self.get_analytical_potential_earth()
        
        modeled = self.integrate_nodal_field('grav_potential')
        if np.min(modeled) > 0: modeled *= -1
        analyt  = self.integrate_nodal_field('grav_potential_analytical')
        diff = np.sum((modeled - analyt))/np.sum(analyt)*100
        print(f'Diff between modelled and analyt potentials: {diff}%',)
        return diff

    def integrate_nodal_field(self, field, mesh="earthmesh"):
        if mesh == "earthmesh":
            mm = get_mass_matrix(self.earthmesh)
            return np.sum(self.earthmesh.element_nodal_fields[field]*mm) 
    def compare_grav_pot_analyt_synth_earth(self):
        self.read_simulation_results()
        if "grav_potential_analytical" not in self.fields:
            get_analytical_potential(self.mesh, self.MODEL, self.config.const.R_Earth)
        if "mass_matrix" not in self.fields:
            self.get_mass_matrix()
        if not hasattr(self, "earthmesh"):
            self.get_onlyEarth_mesh()
        synthetic_solution_integr  = np.abs(np.sum(self.earthmesh.element_nodal_fields['mass_matrix'] * self.earthmesh.element_nodal_fields['grav_potential']))
        analytical_solution_integr = np.abs(np.sum(self.earthmesh.element_nodal_fields['mass_matrix'] * self.earthmesh.element_nodal_fields['grav_potential_analytical']))
        proport_diff_perc = (synthetic_solution_integr - analytical_solution_integr)/analytical_solution_integr*100
        print(f"analytical solution: {analytical_solution_integr },\nsynthetic solution: {synthetic_solution_integr},\nrelative diff (%): {proport_diff_perc}")
        return proport_diff_perc
    
    def plot_grav_pot_analyt_synth_1d(self, lon=0., lat=0.):
        print("check autoreload 2")
        self.read_simulation_results()
        if "grav_potential_analytical" not in self.fields:
            get_analytical_potential(self.mesh, self.MODEL, self.config.const.R_Earth)

        ds_extract_3d_sph = xr.Dataset(
            coords={
                "longitude":  np.linspace(lon, 1., 1),
                "latitude":   np.linspace(lat, 1., 1),
                "depth":      np.linspace(0., self.oneD_solution.radii_relative[-1]*6371e3, 1000),
            },
            attrs={"radius_in_meters": 6371e3},
        )
        ds_extract_1d_synthetic = extract_model_to_regular_grid(
            self.mesh, ds_extract_3d_sph, ['grav_potential'], verbose=True,
            max_tree_doublings = 'auto',
        )
        ds_extract_1d_analytical = extract_model_to_regular_grid(
            self.mesh, ds_extract_3d_sph, ['grav_potential_analytical'], verbose=True,
            max_tree_doublings = 'auto',
        )

        fig, axes = plt.subplots(1,2, sharey=True, figsize=(15,10))
        axes[0].semilogy((-1)*ds_extract_1d_analytical['grav_potential_analytical'][0,0,:],  
                     ds_extract_1d_analytical['depth'],
                    label="analytical")
        axes[0].semilogy(ds_extract_1d_synthetic['grav_potential'][0,0,:],  
                     ds_extract_1d_synthetic['depth'],
                    label="synthetic")
        axes[0].legend()
        axes[0].set_xlabel("grav potential")
        axes[0].set_ylabel("radius (m)")
        
        axes[1].semilogy((-1)*
             ds_extract_1d_analytical['grav_potential_analytical'][0,0,:]-
             ds_extract_1d_synthetic['grav_potential'][0,0,:],  
             ds_extract_1d_synthetic['depth'],
            label="analytical")
        axes[1].set_xlabel("difference")
