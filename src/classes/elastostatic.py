from octiload_class import *

class elastostatic_solver(octiload):             
    def prepare_mesh_fields(self, rho_water=1030., real_or_imag ='re'):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        assert (real_or_imag in ['re', 'im'])
        mesh = self.mesh
        self.rho_water = rho_water
        
        mesh.attach_field('fluid', np.zeros(mesh.nelem))
        mesh.attach_field('zeros', np.zeros((mesh.nelem, mesh.nodes_per_element)))
        mesh.map_nodal_fields_to_element_nodal()
        
        if real_or_imag == "re":
            NEUMANN = -1 * self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag] * self.mesh.elemental_fields['g']
        elif real_or_imag == "im":
            #imaginary part has a pastward imaginary part - the sign has to be swapped
            #self.mesh.elemental_fields['tidal_elevation_'+real_or_imag] *= (-1)
            NEUMANN = -1 * self.rho_water * self.mesh.elemental_fields['tidal_elevation_'+real_or_imag] * self.mesh.elemental_fields['g']
        mesh.attach_field('NEUMANN', NEUMANN)


    def read_gps_data(self, path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index,stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        self.stations = receivers
        
    
    def run_simulation(self, stations=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        mesh                                             = self.mesh
        assert ("r0" in mesh.side_sets.keys())
        assert ("r1" in mesh.side_sets.keys())
        sim                                              = sn.simple_config.simulation.Elastostatic(mesh=mesh)
        sim.domain.polynomial_order                      = mesh.shape_order
        
        sim.physics.elastostatic_equation.gravity = "full"
        sim.physics.elastostatic_equation.right_hand_side.filename = self.meshfile
        sim.physics.elastostatic_equation.right_hand_side.format   = "hdf5"
        sim.physics.elastostatic_equation.right_hand_side.fields   = ["zeros", "zeros", "zeros"]

        sim.physics.elastostatic_equation.solution.filename        = os.path.basename(self.config.elastos_run.volume_solution)
        sim.physics.elastostatic_equation.solution.fields          = self.config.elastos_run.volume_solution_fields
        
        boundaries = simple_config.boundary.Neumann(
            side_sets=["r1"]
        )
        sim.add_boundary_conditions(boundaries)
        
        boundaries = simple_config.boundary.HomogeneousDirichlet(
            side_sets=['r0']
        )
        sim.add_boundary_conditions(boundaries)

        sim.solver.max_iterations     = self.config.elastos_run.max_iterations
        sim.solver.absolute_tolerance = self.config.elastos_run.absolute_tolerance
        sim.solver.relative_tolerance = self.config.elastos_run.relative_tolerance
        sim.solver.preconditioner     = self.config.elastos_run.preconditioner
        
        if hasattr( stations, '__iter__') * (len(stations)>0):
            sim.add_receivers(stations)
            sim.output.point_data.format                          = "hdf5"
            sim.output.point_data.filename                        = os.path.basename(self.config.elastos_run.point_solution)
            sim.output.point_data.sampling_interval_in_time_steps = sim.solver.max_iterations 
        sim.validate()

        sn.api.run(
            input_file    = sim,
            site_name     = self.config.run.site_name,
            output_folder = self.config.elastos_run.solution_folder,
            overwrite     = True,
            ranks         = self.config.run.ranks,
            wall_time_in_seconds = self.config.run.wall_time_in_seconds
        )
        
        return sim
        
    def read_simulation_results(self, field_name_prefix = "solution"):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        path     = self.config.elastos_run.volume_solution
        solution = sn.UnstructuredMesh.from_h5(path)
        displacement = np.zeros((solution.nelem, solution.nodes_per_element, 3))
        for i, comp in enumerate(['_X', '_Y', '_Z']):
            displacement[:,:,i] = solution.element_nodal_fields[field_name_prefix + comp]
        return displacement
    
    def get_gps_stations_from_csv(self, path= "../gps_stations/with_LoadDef/Observed_OTL-induced_Displacements_M2.txt"):
        with open(path) as file:
            header   = file.readline()
            stations = pd.read_csv(file, delim_whitespace=True, index_col=0, header=None, names=header.strip().split(' | ') )

        receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                             side_set_name='r1', station_code=name, fields=["displacement"]) 
                                             for name, lat, lon in zip(stations.index, stations['Lat(+N,deg)'], stations['Lon(+E,deg)']) ]
        return receivers
    