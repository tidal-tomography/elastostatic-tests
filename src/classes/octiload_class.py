import numpy as np
import os
import matplotlib.pyplot as plt
plt.style.use('ggplot')
import pandas as pd
import h5py
import sys
import math

from salvus.mesh.models_1D import model 
from salvus.mesh import simple_mesh
from salvus.flow import api
import salvus.namespace as sn
import salvus.mesh
from salvus.flow import api, simple_config

from mass.elemental_matrices import get_mass_matrix

import classes.config_class as config_class

#ocean tides load modelclass config:
def get_analytical_potential(mesh, MODEL, R_Earth):
    m = model.built_in(MODEL)
    potential_field = m.get_gravitational_potential(np.linalg.norm(mesh.points, axis=1)/R_Earth)[mesh.connectivity]
    mesh.attach_field('grav_potential_analytical', potential_field)

class config:
    def __init__(self):
        self.const        = config_class .const()
        self.input_files  = config_class .input_files()
        self.run          = config_class .run()
        self.gravity_run  = config_class .gravity_run()
        self.elastos_run  = config_class .elastos_run()

class oneD:
    def __init__(self, MODEL, R_Earth, N_points=10, max_radius=1.):
        m             = model.built_in(MODEL)
        self.name     = MODEL
        self.radii_relative = np.logspace(5,np.log10(max_radius), N_points)/R_Earth
        self.grav_pot = m.get_gravitational_potential(self.radii_relative)
        self.grav_acc = m.get_gravity(self.radii_relative)
        self.cmb_depth   = m.discontinuities[2] * R_Earth
        self.icore_depth = m.discontinuities[1] * R_Earth

class octiload:
    def __init__(self, meshfile = '../meshes/mesh.h5'):
        self.config   = config()
        self.meshfile = meshfile
        if not os.path.exists(os.path.dirname(meshfile)): os.makedirs(os.path.dirname(meshfile))
        
    def construct_mesh(self, with_surface_topo = False, with_moho_topo = False, 
                       nex = 18, buffer = 20, dr_basis = 1.5, oneD_model = "prem_iso_one_crust",
                       local_refinement_level = 0, global_refinement_level = 0):
        
        assert type(with_surface_topo)   == bool
        assert type(with_moho_topo)      == bool
        assert type(nex)                 == int
        assert type(buffer)              == int
        
        self.MODEL                    = oneD_model
        self.buffer                   = buffer
        self.with_surface_topo        = with_surface_topo
        self.with_moho_topo           = with_moho_topo
        self.oneD_model               = oneD_model
        self.local_refinement_level   = local_refinement_level
        self.global_refinement_level  = global_refinement_level
        self.dr_basis                 = dr_basis
        
        ms = simple_mesh.TidalLoading()
        ms.basic.tidal_loading_file   = self.config.input_files.ocean_tides_model
        ms.basic.tidal_loading_lmax_1 = self.config.gravity_run.tidal_loading_lmax_1
        ms.basic.tidal_loading_lmax_2 = self.config.gravity_run.tidal_loading_lmax_2
        ms.basic.nex = nex
        ms.basic.local_refinement_level  = local_refinement_level
        ms.basic.global_refinement_level = global_refinement_level
        ms.basic.refinement_threshold    = 0.05
        ms.basic.model = oneD_model

        ms.advanced.tensor_order = self.config.run.tensor_order
        
        if buffer > 0:
            ms.gravity_mesh.add_exterior_domain  = True
            ms.gravity_mesh.nelem_buffer_surface = 2
            ms.gravity_mesh.nelem_buffer_outer   = buffer
            ms.gravity_mesh.dr_basis             = dr_basis

            
        if with_surface_topo:
            assert(exists(self.config.input_files.surface_topography))
            ms.topography.topography_file    = self.config.input_files.surface_topography
            ms.topography.topography_varname = 'topography_earth2014_egm2008_lmax_256_lmax_256'
        if with_moho_topo:
            assert(exists(self.config.input_files.moho_topography))
            ms.topography.moho_topography_file    = self.config.input_files.moho_topography
            ms.topography.moho_topography_varname = 'moho_topography_crust_1_0_egm2008_lmax_256'

        self.mesh = ms.create_mesh(verbose=True)
        self.mesh_array_shape = (self.mesh.nelem, self.mesh.nodes_per_element)
        self.fields = self.mesh.elemental_fields.keys() 
        #removing all unnessesary fields
        del self.mesh.element_nodal_fields['QMU']
        del self.mesh.element_nodal_fields['QKAPPA']
    
    def read_gps_stations(self, path):
        '''Reads info about gps stations either from one file or from the whole directory. 
            par: path - path to file or directory. All files in the directory will be read. '''
        if os.path.isfile(path):
            stations = pd.read_csv(path, delim_whitespace=True)
            receivers = [simple_config.receiver.seismology.SideSetPoint3D(latitude=lat, longitude=lon, depth_in_m=0, 
                                                 side_set_name='r1', station_code=name, fields=self.config.gravity_run.point_solution_fields) 
                                                 for name, lat, lon, depth in zip(stations['Station'],stations['Lat'], stations['Lon'], stations['Height(m)']) ]
        self.stations = receivers
    
    def find_boundary_centre(self, tolerance=200000.):
        def dist(x):
            return np.linalg.norm(x, axis=-1)
        self.mesh.find_side_sets_generic("r0", dist, tolerance=tolerance)
        print('r0 has been found') if "r0" in self.mesh.side_sets.keys() else print('Resolution is not good enough to find this surface. Increase the tolerance')
        
    def write_mesh(self, path=''):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        self.mesh.write_h5(path)
        print(f'the mesh has been written to {path}')
    
    def get_mass_matrix(self):
        assert hasattr(self, "mesh"),  "There is no mesh in the object. Run 'construct_mesh()' first"
        self.mass_matrix = get_mass_matrix(self.mesh)
        self.mesh.attach_field("mass_matrix", self.mass_matrix)
    